output "private-subnet-id" {
  value = module.vpc.private-subnet-id
}

output "vpc-id" {
  value = module.vpc.vpc-id
}
