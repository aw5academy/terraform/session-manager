data "aws_iam_policy_document" "ec2" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "template_file" "ec2-policy" {
  template = file("${path.module}/templates/ec2-policy.json.tpl")
  vars = {
    aws_account_id = data.aws_caller_identity.current.account_id
    kms_key_id     = aws_kms_key.session-manager.id
  }
}

resource "aws_iam_policy" "session-manager" {
  description = "session-manager"
  name        = "session-manager"
  policy      = data.template_file.ec2-policy.rendered
}

resource "aws_iam_role" "session-manager" {
  assume_role_policy = data.aws_iam_policy_document.ec2.json
  name               = "session-manager"
  tags = {
    Name = "session-manager"
  }
}

resource "aws_iam_instance_profile" "session-manager" {
  name  = "session-manager"
  role  = aws_iam_role.session-manager.name
}

resource "aws_iam_role_policy_attachment" "session-manager" {
  policy_arn = aws_iam_policy.session-manager.arn
  role       = aws_iam_role.session-manager.name
}
